import 'package:flutter/material.dart';

// The is the Start Point of All Our Flutter Apps.
void main() {
  runApp(
    // Material App Gives You Material Design Of Your App
    MaterialApp(
      debugShowCheckedModeBanner: false,
      // This Home takes The First Screen Starts Of Your Screen.
      // Text Widget Is Responsible For Displaying Text And Styling Them.
      // The Center Widget is Responsible for Layout Where his child to put.
      home:  Scaffold(
        backgroundColor: Colors.blueGrey,
        appBar: AppBar(
          backgroundColor: Colors.blueGrey[900],
          title: Text("I Am Rich"),
          centerTitle: true,
        ),

        body: Center(
          child: Image(
            image: AssetImage("images/diamond.png"),
          ),
        ),
      ) ,
    )
  );
}

